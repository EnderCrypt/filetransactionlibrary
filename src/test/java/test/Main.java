package test;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;

import endercrypt.library.filetransaction.FileTransactionManager;
import endercrypt.library.filetransaction.exception.FileTransactionCancelledException;
import endercrypt.library.filetransaction.exception.FileTransactionException;
import endercrypt.library.filetransaction.write.FileTransaction;

public class Main
{
	public static void main(String[] args)
	{
		try (FileTransaction fileTransaction = FileTransactionManager.openTransaction())
		{
			try (OutputStream output = fileTransaction.open(Paths.get("test/transaction 1.txt")))
			{
				for (int i = 0; i < 1024; i++)
				{
					output.write("transaction 1\n".getBytes());
				}
			}

			try (OutputStream output = fileTransaction.open(Paths.get("test/transaction 2.txt")))
			{
				for (int i = 0; i < 1024; i++)
				{
					output.write("transaction 2\n".getBytes());
				}
			}
		}
		catch (FileTransactionCancelledException e)
		{
			System.err.println("transaction cancelled");
		}
		catch (FileTransactionException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
