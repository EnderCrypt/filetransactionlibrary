package endercrypt.library.filetransaction.write;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

import endercrypt.library.filetransaction.exception.FileTransactionCancelledException;
import endercrypt.library.filetransaction.exception.FileTransactionException;
import endercrypt.library.filetransaction.touch.TouchFile;
import endercrypt.library.filetransaction.util.FileTransactionPath;
import endercrypt.library.filetransaction.util.FileTransactionUtility;

public class TransactionOutputStream extends OutputStream
{
	private final FileTransaction fileTransaction;
	private final FileTransactionPath path;

	private final FileOutputStream fileOutputStream;

	private final OutputStream digestOutput;
	private final DigestOutputStream digestOutputStream;

	private boolean stable = true;
	private boolean closed = false;

	protected TransactionOutputStream(FileTransaction fileTransaction, Path path) throws FileNotFoundException, FileTransactionException
	{
		// transaction
		this.fileTransaction = fileTransaction;

		// path
		this.path = new FileTransactionPath(path);

		// file
		if (Files.exists(path().buffer()) || Files.exists(path().meta()))
		{
			throw new FileTransactionException(path().real() + " already has an existing transaction going on");
		}
		this.fileOutputStream = new FileOutputStream(path().buffer().toString());

		// hash
		this.digestOutput = new ByteArrayOutputStream();
		DigestOutputStream digestOutputStreamVariable = null;
		try
		{
			digestOutputStreamVariable = FileTransactionUtility.getHashStream(this.digestOutput);
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new FileTransactionException("Failed to initialize hash alghoritm " + FileTransactionUtility.getHashAlgorithm(), e);
		}
		this.digestOutputStream = digestOutputStreamVariable;
	}

	public FileTransactionPath path()
	{
		return this.path;
	}

	public boolean isCancelled()
	{
		return this.fileTransaction.isCancelled();
	}

	public boolean isStable()
	{
		return this.stable;
	}

	public boolean isClosed()
	{
		return this.closed;
	}

	@Override
	public void write(int b) throws IOException
	{
		if (isCancelled())
		{
			throw new FileTransactionCancelledException();
		}
		try
		{
			this.fileOutputStream.write(b);

			this.digestOutputStream.write(b);
		}
		catch (Exception e)
		{
			this.stable = false;
			throw e;
		}
	}

	@Override
	public void close() throws IOException
	{
		if (isCancelled())
		{
			throw new FileTransactionCancelledException();
		}
		if (isClosed())
		{
			throw new IOException(FileTransaction.class.getSimpleName() + " has already been closed");
		}
		if (isStable())
		{
			try
			{
				this.fileOutputStream.close();
				this.closed = true;
			}
			catch (Exception e)
			{
				this.stable = false;
				throw e;
			}
		}
	}

	protected void finalizeTransaction(UUID uuid, Collection<TransactionOutputStream> transactions) throws FileNotFoundException, IOException
	{
		if (isCancelled())
		{
			throw new FileTransactionCancelledException();
		}
		if (transactions.stream().filter(tos -> tos.path().equals(path()) == true).findAny().isPresent() == false)
		{
			throw new FileTransactionException("A file transaction did not contain itself");
		}

		TouchFile touchFile = new TouchFile(this.path, uuid, FileTransactionUtility.getHashAlgorithm(), this.digestOutputStream.getMessageDigest().digest(), transactions.stream().map(tos -> tos.path().real()).collect(Collectors.toList()));
		touchFile.write();
	}
}
