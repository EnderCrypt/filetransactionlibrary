package endercrypt.library.filetransaction.write;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import endercrypt.library.filetransaction.FileTransactionManager;
import endercrypt.library.filetransaction.exception.FileTransactionCancelledException;
import endercrypt.library.filetransaction.exception.FileTransactionException;

public class FileTransaction implements Closeable
{
	private final UUID uuid;
	private List<TransactionOutputStream> streams = new ArrayList<>();

	private boolean cancelled = false;

	public FileTransaction()
	{
		this.uuid = UUID.randomUUID();
	}

	public OutputStream open(Path path) throws IOException
	{
		FileTransactionManager.touch(path);

		TransactionOutputStream tos = new TransactionOutputStream(this, path);
		this.streams.add(tos);
		return tos;
	}

	public void cancel()
	{
		this.cancelled = true;
	}

	public boolean isCancelled()
	{
		return this.cancelled;
	}

	@Override
	public void close() throws IOException
	{
		try
		{
			// check that there's been any transactions
			if (this.streams.size() == 0)
			{
				// System.err.println("WARNING: there were no transactions");
				return;
			}

			// close all streams
			for (TransactionOutputStream tos : this.streams)
			{
				if (tos.isClosed() == false)
				{
					// System.err.println("WARNING: "+tos+" was not closed");
					tos.close();
				}
			}

			// check if any streams crashed
			for (TransactionOutputStream tos : this.streams)
			{
				if (tos.isStable() == false)
				{
					throw new FileTransactionException("a transaction was marked as not stable");
				}
			}

			// finalize transactions
			for (TransactionOutputStream tos : this.streams)
			{
				tos.finalizeTransaction(this.uuid, this.streams);
			}

		}
		catch (FileTransactionCancelledException e)
		{
			// ignore
		}
		finally
		{
			// touch files
			this.streams.stream().map(tos -> tos.path().real()).forEach(FileTransactionManager::touch);
			//FileTransactionManager.touch(this.streams.iterator().next().path().real());
		}
	}
}
