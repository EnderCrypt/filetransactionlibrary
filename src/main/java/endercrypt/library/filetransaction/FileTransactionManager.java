package endercrypt.library.filetransaction;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

import endercrypt.library.filetransaction.exception.FileTransactionTouchException;
import endercrypt.library.filetransaction.touch.FileTransactionTouch;
import endercrypt.library.filetransaction.util.FileTransactionPath;
import endercrypt.library.filetransaction.util.FileTransactionUtility;
import endercrypt.library.filetransaction.write.FileTransaction;

public class FileTransactionManager
{

	public static boolean touch(Path file)
	{
		try
		{
			FileTransactionTouch fileTransactionTouch = new FileTransactionTouch(file);
			fileTransactionTouch.applyTransaction();
			return true;
		}
		catch (FileTransactionTouchException e)
		{
			return false;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			FileTransactionPath ftp = new FileTransactionPath(file);
			FileTransactionUtility.attemptDelete(ftp.meta());
			FileTransactionUtility.attemptDelete(ftp.buffer());
		}
	}

	public static InputStream openRead(Path file) throws IOException
	{
		touch(file);
		return new FileInputStream(file.toString());
	}

	public static FileTransaction openTransaction()
	{
		return new FileTransaction();
	}
}
