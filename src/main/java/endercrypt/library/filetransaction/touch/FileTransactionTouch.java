package endercrypt.library.filetransaction.touch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import endercrypt.library.filetransaction.exception.FileTransactionTouchException;
import endercrypt.library.filetransaction.util.FileTransactionPath;
import endercrypt.library.filetransaction.util.FileTransactionUtility;

public class FileTransactionTouch
{
	private List<TouchFile> touchFiles = new ArrayList<>();

	public FileTransactionTouch(Path file) throws IOException
	{
		// load all touch files
		TouchFile indexTouchFile = new TouchFile(file);
		for (Path path : indexTouchFile.getTransactions())
		{
			this.touchFiles.add(new TouchFile(path));
		}

		// verify
		try
		{
			for (TouchFile touchFile : this.touchFiles)
			{
				if (touchFile.getUuid().equals(indexTouchFile.getUuid()) == false)
				{
					throw new FileTransactionTouchException("touch file uuid not equal");
				}
				if (touchFile.getHashAlgorithm().equals(indexTouchFile.getHashAlgorithm()) == false)
				{
					throw new FileTransactionTouchException("touch file hash algorithm does not match");
				}
				if ((touchFile.getTransactions().containsAll(indexTouchFile.getTransactions()) && indexTouchFile.getTransactions().containsAll(touchFile.getTransactions())) == false)
				{
					throw new FileTransactionTouchException("touch file transactions does not match");
				}
				if (touchFile.verifyDigest(touchFile.getPath().buffer()) == false)
				{
					throw new FileTransactionTouchException("file does not match hash");
				}
			}
		}
		catch (FileTransactionTouchException e)
		{
			delete();
			throw e;
		}
	}

	public void delete()
	{
		for (TouchFile touchFile : this.touchFiles)
		{
			FileTransactionPath path = touchFile.getPath();
			FileTransactionUtility.attemptDelete(path.buffer());
			FileTransactionUtility.attemptDelete(path.meta());
		}
	}

	public void applyTransaction() throws FileTransactionTouchException
	{
		try
		{
			// delete original files
			for (TouchFile touchFile : this.touchFiles)
			{
				Path realPath = touchFile.getPath().real();
				if (Files.exists(realPath))
				{
					Files.delete(realPath);
				}
			}

			// copy buffer
			for (TouchFile touchFile : this.touchFiles)
			{
				FileTransactionPath path = touchFile.getPath();
				Files.copy(path.buffer(), path.real());
			}

			// verify copy
			for (TouchFile touchFile : this.touchFiles)
			{
				FileTransactionPath path = touchFile.getPath();
				if (touchFile.verifyDigest(path.real()) == false)
				{
					throw new FileTransactionTouchException("buffer copy had wrong hash");
				}
			}

			// delete transaction
			delete();
		}
		catch (FileTransactionTouchException e)
		{
			throw e;
		}
		catch (IOException e)
		{
			throw new FileTransactionTouchException(e);
		}
	}
}
