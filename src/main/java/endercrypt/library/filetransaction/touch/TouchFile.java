package endercrypt.library.filetransaction.touch;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang.ArrayUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import endercrypt.library.filetransaction.exception.FileTransactionException;
import endercrypt.library.filetransaction.exception.FileTransactionTouchException;
import endercrypt.library.filetransaction.util.FileTransactionPath;
import endercrypt.library.filetransaction.util.FileTransactionUtility;

public class TouchFile
{
	private final FileTransactionPath path;

	private final UUID uuid;
	private final String hashAlgorithm;
	private final byte[] hashDigest;
	private final Collection<Path> transactions;

	public TouchFile(FileTransactionPath path, UUID uuid, String hashAlgorithm, byte[] hashDigest, Collection<Path> transactions)
	{
		this.path = path;

		this.uuid = uuid;
		this.hashAlgorithm = hashAlgorithm;
		this.hashDigest = hashDigest;
		this.transactions = transactions;
	}

	public TouchFile(Path file) throws IOException
	{
		this.path = new FileTransactionPath(file);
		if (Files.exists(this.path.buffer()) == false || Files.exists(this.path.meta()) == false)
		{
			throw new FileTransactionTouchException("expected touch file " + file + " did not exists");
		}

		String jsonString = new String(Files.readAllBytes(this.path.meta()));
		JsonObject root = FileTransactionUtility.getGson().fromJson(jsonString, JsonObject.class);

		this.uuid = UUID.fromString(root.get("uuid").getAsString());
		JsonObject hashRoot = root.getAsJsonObject("hash").getAsJsonObject();
		this.hashAlgorithm = hashRoot.get("algorithm").getAsString();
		JsonArray hashDigestArray = hashRoot.get("digest").getAsJsonArray();
		this.hashDigest = ArrayUtils.toPrimitive(StreamSupport.stream(hashDigestArray.spliterator(), false).map(je -> je.getAsByte()).toArray(Byte[]::new));
		List<Path> localTransactions = StreamSupport.stream(root.get("transactions").getAsJsonArray().spliterator(), false).map(e -> e.getAsString()).map(s -> Paths.get(s)).collect(Collectors.toList());

		this.transactions = Collections.unmodifiableList(localTransactions);
	}

	public FileTransactionPath getPath()
	{
		return this.path;
	}

	public UUID getUuid()
	{
		return this.uuid;
	}

	public String getHashAlgorithm()
	{
		return this.hashAlgorithm;
	}

	public byte[] getHashDigest()
	{
		return this.hashDigest;
	}

	public boolean verifyDigest(Path file) throws IOException
	{
		try
		{
			MessageDigest hasher = FileTransactionUtility.getHasher();
			hasher.update(Files.readAllBytes(file));
			byte[] hash = hasher.digest();
			return Arrays.equals(hash, this.hashDigest);
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new FileTransactionException("unable to get hash alghoritm " + FileTransactionUtility.getHashAlgorithm(), e);
		}
	}

	public Collection<Path> getTransactions()
	{
		return this.transactions;
	}

	public void write() throws FileNotFoundException, IOException
	{
		JsonObject root = new JsonObject();

		// uuid
		root.addProperty("uuid", this.uuid.toString());

		// hash
		JsonObject hashRoot = new JsonObject();
		hashRoot.addProperty("algorithm", FileTransactionUtility.getHashAlgorithm());
		hashRoot.add("digest", FileTransactionUtility.byteArrayToJson(this.hashDigest));
		root.add("hash", hashRoot);

		// transactions
		JsonArray transactionArray = new JsonArray();
		this.transactions.stream().map(p -> p.toString()).forEach(transactionArray::add);
		root.add("transactions", transactionArray);

		// save
		try (OutputStream output = new FileOutputStream(getPath().meta().toString()))
		{
			output.write(FileTransactionUtility.getGson().toJson(root).getBytes());
		}
	}
}
