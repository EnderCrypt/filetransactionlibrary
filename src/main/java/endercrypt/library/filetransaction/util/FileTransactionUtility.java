package endercrypt.library.filetransaction.util;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

public class FileTransactionUtility
{
	private static final Gson gson;
	static
	{
		gson = new GsonBuilder()
				.create();
	}

	public static Gson getGson()
	{
		return gson;
	}

	public static final String hashAlgorithm = "MD5";

	public static String getHashAlgorithm()
	{
		return hashAlgorithm;
	}

	public static MessageDigest getHasher() throws NoSuchAlgorithmException
	{
		return MessageDigest.getInstance(getHashAlgorithm());
	}

	public static DigestOutputStream getHashStream(OutputStream stream) throws NoSuchAlgorithmException
	{
		return new DigestOutputStream(stream, getHasher());
	}

	public static void attemptDelete(Path file)
	{
		try
		{
			if (Files.exists(file) && Files.isRegularFile(file))
			{
				Files.delete(file);
			}
		}
		catch (IOException e)
		{
			// ignore
		}
	}

	public static JsonArray byteArrayToJson(byte[] array)
	{
		JsonArray json = new JsonArray();
		Arrays.asList(ArrayUtils.toObject(array)).stream().map(b -> (Number) b).forEach(json::add);
		return json;
	}
}
