package endercrypt.library.filetransaction.util;

import java.nio.file.Path;

public class FileTransactionPath
{
	private static final boolean UNIX_DOT = true;
	private static final String BUFFER_EXTENSION = "transaction.buffer";
	private static final String META_EXTENSION = "transaction.meta";

	private final Path path;
	private final Path bufferPath;
	private final Path metaPath;

	public FileTransactionPath(Path path)
	{
		this.path = path.toAbsolutePath();
		this.bufferPath = create(this.path, BUFFER_EXTENSION);
		this.metaPath = create(this.path, META_EXTENSION);
	}

	public static Path create(Path path, String extension)
	{
		Path parent = path.getParent();
		String filename = path.getFileName().toString();
		String dot = UNIX_DOT ? "." : "";
		return parent.resolve(dot + filename + "." + extension);
	}

	public Path real()
	{
		return this.path;
	}

	public Path buffer()
	{
		return this.bufferPath;
	}

	public Path meta()
	{
		return this.metaPath;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.path == null) ? 0 : this.path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		FileTransactionPath other = (FileTransactionPath) obj;
		if (this.path == null)
		{
			if (other.path != null) return false;
		}
		else if (!this.path.equals(other.path)) return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "FileTransactionPath [path=" + this.path + ", bufferPath=" + this.bufferPath + ", metaPath=" + this.metaPath + "]";
	}
}
