package endercrypt.library.filetransaction.exception;

import java.io.IOException;

@SuppressWarnings("serial")
public class FileTransactionException extends IOException
{
	public FileTransactionException()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public FileTransactionException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FileTransactionException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public FileTransactionException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
}
