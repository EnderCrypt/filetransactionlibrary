package endercrypt.library.filetransaction.exception;

@SuppressWarnings("serial")
public class FileTransactionCancelledException extends FileTransactionException
{
	public FileTransactionCancelledException()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public FileTransactionCancelledException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FileTransactionCancelledException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public FileTransactionCancelledException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
}
