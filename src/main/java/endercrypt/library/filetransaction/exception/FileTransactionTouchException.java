package endercrypt.library.filetransaction.exception;

@SuppressWarnings("serial")
public class FileTransactionTouchException extends FileTransactionException
{
	public FileTransactionTouchException()
	{
		// TODO Auto-generated constructor stub
	}

	public FileTransactionTouchException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FileTransactionTouchException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public FileTransactionTouchException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
}
